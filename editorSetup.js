import { EditorState, basicSetup } from "./_snowpack/pkg/@codemirror/basic-setup.js";
import { EditorView, keymap } from "./_snowpack/pkg/@codemirror/view.js";
import { defaultTabBinding } from "./_snowpack/pkg/@codemirror/commands.js";
import {json} from "./_snowpack/pkg/@codemirror/lang-json.js"

export default function editorSetups() {
  const requestBody = document.querySelector("[data-request-body]");
  const responseBody = document.querySelector("[data-response-body]");

  const Extensions = [
    basicSetup,
    keymap.of([defaultTabBinding]),
    json(),
    EditorState.tabSize.of(2),
  ];

  const requestEditor = new EditorView({
    state: EditorState.create({
      doc: "{\n\t\n}",
      extensions: Extensions,
    }),
    parent: requestBody,
  });

  const responseEditor = new EditorView({
    state: EditorState.create({
      doc: "{}",
      extensions: [...Extensions, EditorView.editable.of(false)],
    }),
    parent: responseBody,
  });

  const updateResponse = (value) => {
    responseEditor.dispatch({
      changes: {
        from: 0,
        to: responseEditor.state.doc.length,
        insert: JSON.stringify(value, null, 2),
      },
    });
  };

  return { requestEditor, updateResponse };
}
